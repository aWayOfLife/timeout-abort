import fetch from 'node-fetch';
import AbortController from 'abort-controller';

const apiUrl = 'http://localhost:8000/api/health/timeout'; // Replace with your actual API endpoint

async function makeRequest() {
  try {
    const requestBody = { key: 'value' }; // Replace with your actual request body
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    };

    const timeoutDuration = 70000; // Timeout in milliseconds (3 seconds)
    const controller = new AbortController();
    const timeoutId = setTimeout(() => {
      controller.abort();
    }, timeoutDuration);

    const response = await fetch(apiUrl, {
      ...requestOptions,
      signal: controller.signal,
    });

    clearTimeout(timeoutId);

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const healthData = await response.json();
    console.log('Health Data:', healthData);
  } catch (error) {
    console.log(error.name);
    console.error('Error:', error.message);
  }
}

makeRequest();
